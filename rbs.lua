#!/usr/bin/env luajit

local lfs_loaded, lfs = pcall(function() return require 'lfs' end)

--@TODO(Renzix): optional logging
function log(str)
  print("LOG: " .. str)
end

if not lfs_loaded then
  log("Warning luafilesystem not loaded")
end

local hilbish_loaded = nil
if hilbish ~= nil then
  log("Hilbish loaded")
  hilbish_loaded = true
else
  hilbish_loaded = false
end

local rbs = {}

rbs.cl_commands = {}

-- @TODO(Renzix): Windows support
-- @TODO(Renzix): Differentiate between local and global libs
-- @TODO(Renzix): Be able to find local libraries from specified path?
-- @TODO(Renzix): Maybe install library to local from github? (might be out of scope)
-- @TODO(Renzix): Add more language support (not just c)
-- @TODO(Renzix): Function to call other rbs files
-- @TODO(Renzix): Reorganize all these functions
-- @TODO(Renzix): Support whitespace in input/output
-- @TODO(Renzix): Add linux/windows asserts
-- @TODO(Renzix): Setup a way to profile stuff
-- @TODO(Renzix): get giant 2k+ c file example
-- @TODO(Renzix): read symbols in shared lib to see what changed
-- @TODO(Renzix): add b.make which combs commands for outputs/inputs?
-- @TODO(Renzix): async stuff maybe?

function rbs.shell(cmd_str)
  if hilbish_loaded then
    local exit_code, stdout, stderr = hilbish.run(cmd_str, true)
    return exit_code, stdout or stderr
  else
    local handle = io.popen(cmd_str)
    local ret = handle:read("*a")
    handle:close()
    return ret
  end
end

function rbs.pkg_config(libs, link)
  local link_str = ""
  if link == false then
    link_str = "--cflags "
  end
  if libs == nil or libs == "" then
    return ""
  else
    -- TODO(Renzix): print this and deal with errors
    local handle = io.popen("pkg-config --libs " .. link_str .. libs)
    local result = handle:read("*a")
    handle:close()
    return result:gsub("\n", " ")
  end
end

function rbs.to_string(var)
  if (type(var) == "string") then
    return var
  elseif (type(var) == "table") then
    return table.concat(var, " ")
  elseif var == nil then
    return ""
  else
    assert(false, "Not implemented")
  end
end

function rbs.generate_cc_cmd_str(cmd, input, output)
  assert(cmd.cd == nil, "cd not supported for this operation")
  local lib_args = ""
  -- TODO(Renzix): Maybe find a better way to tell if need cflags or not
  if(type(cmd.output) == "string") then
    lib_args = rbs.pkg_config(rbs.to_string(cmd.libs), true)
  elseif type(cmd.output) == "table" then
    lib_args = rbs.pkg_config(rbs.to_string(cmd.libs), false)
  end
  return cmd.bin .. " " .. rbs.to_string(cmd.flags) .. " " .. lib_args ..  " " .. input .. " -o " .. output
end

function rbs.generate_generic_cmd_str(cmd)
  assert(type(cmd.bin) == "string")
  local move = ""
  if type(cmd.cd) == "string" then
    move = "cd " .. cmd.cd .. " && "
  end
  return move .. cmd.bin .. " " .. rbs.to_string(cmd.arg)
end

-- @TODO(Renzix): cache these somehow?
function rbs.is_updated(input, output)
  if lfs_loaded then
    local output_time_modified, out_err = lfs.attributes(output, 'modification')
    local input_time_modified, in_err = lfs.attributes(input, 'modification')
    if in_err == nil and out_err == nil and output_time_modified > input_time_modified then
      return true
    end
  elseif hilbish_loaded then
    -- local output_time_modified = tonumber(hilbish.run("stat -c %Y ".. output))
    -- local input_time_modified = tonumber(hilbish.run("stat -c %Y ".. input))
    -- if input_time_modified ~= nil and output_time_modified ~= nil and output_time_modified > input_time_modified then
      -- return true
    -- end
    return false  -- @TODO(Renzix): make this work
  else
    -- @TODO(Renzix): handle errors and deal with windows
    local handle = io.popen("stat -c %Y " .. output)
    local output_time_modified = tonumber(handle:read("*a"))
    handle = io.popen("stat -c %Y " .. input)
    local input_time_modified = tonumber(handle:read("*a"))
    handle:close()
    if input_time_modified ~= nil and output_time_modified ~= nil and output_time_modified > input_time_modified then
      return true
    end
  end
  return false
end

function rbs.exec_single_output(cmd)
  if not cmd.force then
    -- @TODO(Renzix): test for all inputs
    if cmd.input ~= nil and rbs.is_updated(cmd.input[1], cmd.output) then
      return
    end
  end
  local cmd_str
  if cmd.custom_cmd_str ~= nil then
    cmd_str = cmd.custom_cmd_str(cmd)
  elseif string.find(cmd.bin,"gcc") then
    cmd_str = rbs.generate_cc_cmd_str(cmd, table.concat(cmd.input, " "), cmd.output)
  elseif string.find(cmd.bin, "clang") then
    cmd_str = rbs.generate_cc_cmd_str(cmd, table.concat(cmd.input, " "), cmd.output)
  else
    cmd_str = rbs.generate_generic_cmd_str(cmd)
  end
  log(cmd_str)
  rbs.shell(cmd_str)
end

function rbs.exec_multiple_outputs(cmd)
  for k, _ in pairs(cmd.output) do
    repeat
      -- first check if updated by timestamp -- @TODO(Renzix): test on windows and remove dependency
      if not cmd.force then
        if rbs.is_updated(cmd.input[k],cmd.output[k]) then
          break
        end
      end
      local cmd_str
      if cmd.custom_cmd_str ~= nil then
        cmd_str = cmd.custom_cmd_str(cmd)
      elseif string.find(cmd.bin,"gcc") then
        cmd_str = rbs.generate_cc_cmd_str(cmd, cmd.input[k], cmd.output[k])
      elseif string.find(cmd.bin, "clang") then
        cmd_str = rbs.generate_cc_cmd_str(cmd, cmd.input[k], cmd.output[k])
      else
        cmd_str = rbs.generate_generic_cmd_str(cmd)
      end
      log(cmd_str)
      rbs.shell(cmd_str)
      break
    until true
  end
end

function rbs.exec(cmd)
  assert(type(cmd)=="table", "cmd is not a table")
  assert(type(cmd.bin) == "string", "bin is not valid")
  -- assert(type(cmd.input) == "table", "input is not valid")
  if type(cmd.input) == "table" then
    if type(cmd.output) == "string" then
      rbs.exec_single_output(cmd)
    elseif type(cmd.output) == "table" then
      assert(#cmd.output == #cmd.input, "output table has to be equal to input table or a single string") -- @TODO(Renzix): Support variable # of outputs
      rbs.exec_multiple_outputs(cmd)
    else
      assert(nil, "output is invalid")
    end
  end
  rbs.exec_single_output(cmd)
end

function rbs.bind(cmd, name, func)
  if rbs.cl_commands[name] == nil then
    rbs.cl_commands[name] = {}
    rbs.cl_commands[name][1] = {}
    rbs.cl_commands[name][1][cmd] = func
  else
    local temp_table = {}
    temp_table[cmd] = func
    table.insert(rbs.cl_commands[name], temp_table)
  end
end

function rbs.bind_exec(cmd, name)
  rbs.bind(cmd, name, rbs.exec)
end

function rbs.bind_clean(cmd, name)
  rbs.bind(cmd, name, rbs.clean)
end

function rbs.bind_install(cmd, name)
  rbs.bind(cmd, name, rbs.install)
end

-- @TODO(Renzix): support flags
function rbs.run_arg(arg)
  if type(arg) ~= "table" or not next(arg) then
    arg = { "all" }
  end
  for i, arg_name in pairs(arg) do
    if i < 1 then
      arg[i] = nil
    elseif rbs.cl_commands[arg_name] ~= nil then
      for _, tab in pairs(rbs.cl_commands[arg_name]) do
        for cmd,func in pairs(tab) do
          func(cmd)
        end
      end
    else
      assert(false, "Unknown command: " .. arg_name)
    end
  end
end

function rbs.run(str)
  assert(type(str) == "string")
  local str_tbl = {}
  for value in string.gmatch(str, '([^ *]+)') do
    str_tbl[#str_tbl+1] = value
  end
  rbs.run_arg(str_tbl)
end

function rbs.replace_extension(files, new_extension)
  assert(type(files) == "table")
  assert(type(new_extension) == "string")
  if (type(files) == "table") then
    local new_files = {}
    for k, _ in pairs(files) do
      new_files[k] = string.gsub(files[k], "[.].*", new_extension)
    end
    return new_files
  elseif (type(files) == "string") then
    return string.gsub(files, "[.].*", new_extension)
  end
end

function rbs.table_concat(t1,t2)
  assert(type(t1) == "table")
  assert(type(t2) == "table")
   for i=1,#t2 do
      t1[#t1+1] = t2[i]
   end
   return t1
end

function rbs.get_files(srcdir, ext)
  assert(type(srcdir) == "string")
  assert(type(ext) == "string")
  assert(lfs_loaded == true)
  -- @TODO(Renzix): add POSIX or windows way to get all files with extension?
  local ret = {}
  for file in lfs.dir(srcdir) do
    if file ~= "." and file ~= ".." then
      local filetype = lfs.attributes(srcdir .. "/" .. file, 'mode')
      if (filetype == 'directory') then
        ret = rbs.table_concat(rbs.get_files(srcdir .. "/" .. file, ext), ret)
      elseif string.find(file,ext) then
        table.insert(ret, srcdir .. "/" .. file)
      end
    end
  end
  return ret
end

function dir_of(str)
  if string.find(str, "/") then
    return string.gsub(str, "(.*)(/.*)", "%1")
  else
    return ""
  end
end

function basename(str)
  if string.find(str, "/") then
    return string.gsub(str, "(.*/)(.*)", "%2")
  else
    return str
  end
end

-- @TODO(Renzix): make not recursive and keep track of already known dirs somehow
function mkdirp(ofiles)
  if type(ofiles) == "table" then
    for _, v in pairs(ofiles) do
      mkdirp(dir_of(v))
    end
  elseif type(ofiles) == "string" then
    if hilbish_loaded == true then
      hilbish.run("mkdir -p " .. ofiles)
      return
    elseif lfs_loaded == false then
      -- @TODO(Renzix): handle windows here
      local handle = io.popen("mkdir -p " .. ofiles)
      handle:close()
      return
    end
    if ofiles == "" then return end
    if (lfs.attributes(dir_of(ofiles), "mode") ~= "directory") then
      log("Creating " .. ofiles)
      mkdirp(dir_of(ofiles))
      lfs.mkdir(ofiles)
    else
      log("Creating " .. ofiles)
      lfs.mkdir(ofiles)
    end
  end
end

function rbs.to_ofiles(files, srcdir, objdir)
  assert(type(files) == "table")
  assert(type(srcdir) == "string")
  assert(type(objdir) == "string")
  -- convert extensions
  local ofiles = {}
  ofiles = rbs.replace_extension(files, ".o")
  if srcdir ~= nil then
  -- convert directory
    for k, _ in pairs(files) do
      log(files[k].. "\n")
      ofiles[k] = string.gsub(ofiles[k], srcdir, objdir)
      log(ofiles[k].. "\n")
    end
  end
  mkdirp(ofiles)
  return ofiles
end

function rbs.clean(cmd)
  assert(type(cmd) == "table")
  assert(cmd.output ~= nil)
  if (type(cmd.output) == "string") then
      log("removing " .. cmd.output)
      os.remove(cmd.output)
  elseif (type(cmd.output) == "table") then
    for _, v in pairs(cmd.output) do
      log("removing " .. v)
      os.remove(v)
    end
  else
    log("Could not clean, invalid cmd")
  end
end

function rbs.install(cmd)
  -- install output to srcdir
  assert(type(cmd) == "table", "cmd is not a table")
  assert(type(cmd.output) == "string", "Could not install multiple items")
  prefix = os.getenv("PREFIX") or "/usr/bin"
  if prefix == "" then
    slash = ""
  else
    slash = "/"
  end
  log("installing to " .. prefix .. slash .. basename(cmd.output))
  os.rename(cmd.output, prefix .. slash .. basename(cmd.output) )
end

function rbs.autogen(progname, language, options)
  assert(type(progname) == "string")
  assert(progname:gsub("%s+", "") ~= "")
  if language == "c" then
    local cc = options.cc or "cc"
    local cfiles = options.cfiles or rbs.get_files("src","[.]c")
    local libs = options.libs
    local dir = options.builddir or "build/"
    local ofiles = nil
    if cfiles == nil then
      ofiles = rbs.to_ofiles(cfiles, "src", dir .. "obj")
    else
      local srcprefix = options.srcprefix or "^" -- should be location of the source files without regex. Defaults to current dir
      ofiles = rbs.to_ofiles(cfiles, srcprefix, dir .. "obj/")
    end
    local bin = dir .. progname
    local cflags = options.cflags or {}
    table.insert(cflags,"-c")
    local lflags = options.lflags
    local force = options.force or false

    local compile  = {}
    compile.bin    = cc
    compile.input  = cfiles
    compile.output = ofiles
    compile.libs   = libs
    compile.flags  = cflags
    compile.force  = force

    local link    = {}
    link.bin      = cc
    link.input    = ofiles
    link.libs     = libs
    link.output   = bin
    link.lflags   = lflags
    link.force    = force

    return compile, link
  else
    assert(false, "language not supported yet")
  end

  return {}, {}
end

function rbs.autobind(cmd, name, options)
  assert(type(cmd) == "table")
  assert(type(name) == "string")
  rbs.bind_exec(cmd, "all")
  rbs.bind_exec(cmd, name)
  rbs.bind_clean(cmd, "clean")
  if options ~= nil then
    if options.install == true then
      rbs.bind_install(cmd, "install")
    end
  end
end

return rbs
