# rbs

## Why

I made a build system because
 1. I want it to be fast
 2. I want it to be cross platform
 3. I want no or minimal dependencies
 4. I want it to be simple
 5. I want it to be able to enable/disable features
 
I have not found a build system which does all of these things. 

### Speed

I want it to be fast. How will I make it fast? I will try to do as little things
as possible and use lua. The compiler should always be the bottleneck.

### Cross platform

I want it to work the same on linux as it does on windows as it does on macOS as
it does on any operating system. Fortunately lua is widely supported however its
interactions with the filesystem is very minimal which is unfortunate.

### Minimal Dependencies

I want it to have as little dependencies as possible. Unfortunately I cannot
guarantee that any 1 program is installed so I am going to have (minimum) 1
dependency. The other solutions are only supporting already installed
tools/operating systems (ie bat/sh) or having a binary for a build system and
have the user install it. The first doesn't solve the problem of being cross
platform and the second has already been solved with gnumake among others. Also
you can consider build generators the third but it should be obvious why build
generators are not a good idea(extra step for no reason with multiple
dependencies). With using lua we also have the option of including it into the
git repository meaning the user doesnt even need to install anything extra
(unlike gnu make/other build systems).

### Simple

I want any random programmer to read the build file and intuitively know how it
works. I also want to keep the amount of functions in it as small as possible
and expose internals in case someone wants to do some things more manually.

### Features

I am just gonna list some features (note the tbd means I have thought about it
but currently have no plans for it, i do think it would be cool).

- Compilation (duh)
- Way to cleaning builds
- Only build outdated files by default (can force)
- Language agnostic with 1-2 special function for specific languages
- Way to install program
- Nice command line interface
- Way to run the build system in a interpreter and update/observe it live (tbd)
- Maybe a way to pull other projects and build via git (tbd)
- Way to interact with git ie create git hook (tbd)
