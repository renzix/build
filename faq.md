
# Table of Contents

1.  [Why rbs over X](#orgad1ea22)
    1.  [Meson](#orgd9a28fd)
    2.  [Cmake](#orga19e9b7)
    3.  [Premake](#org8409a59)
    4.  [Make](#org99c2a97)
    5.  [Ninja](#org52e701f)



<a id="orgad1ea22"></a>

# Why rbs over X

Most languages use a DSL and/or require dependencies. There is nothing wrong
with a lot of these but I just want something fast, extendable, simple(not easy
but simple), and has as little deps as possible. Also keep in mind part of this
is just for me to make something good.


<a id="orgd9a28fd"></a>

## Meson

Not extendable and uses a bunch of dependencies (ninja, python, meson itself,
etc&#x2026;). Fine for larger projects where you don&rsquo;t want to do complex things,
there is nothing wrong with that.


<a id="orga19e9b7"></a>

## Cmake

Extendable with DSL (would rather not), complex, uses at least 2 dependencies
(make, cmake itself etc) requires you to install it(not scripting language).


<a id="org8409a59"></a>

## Premake

Too much magic and uses make or other build system.


<a id="org99c2a97"></a>

## Make

Not available on nonunix systems(or if it is, its very jank) and DSL.


<a id="org52e701f"></a>

## Ninja

Not made for humans and requires install.

