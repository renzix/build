#!/usr/bin/env luajit

local b = require 'rbs'

local Test1 = {}
Test1.bin = "./rbs-manual"
Test1.cd  = "example/cprog-simple"
Test1.arg = "all"
local Test2 = {}
Test2.bin = "./rbs-autogen"
Test2.cd  = "example/cprog-simple"
Test2.arg = "all"
local Test3 = {}
Test3.bin = "./rbs-manual"
Test3.cd  = "example/cprog-complex"
Test3.arg = "all"
local Test4 = {}
Test4.bin = "./rbs-autogen"
Test4.cd  = "example/cprog-complex"
Test4.arg = "all"

-- run tests
b.exec(Test1)
b.exec(Test2)
b.exec(Test3)
b.exec(Test4)
