
# Table of Contents

1.  [Manual](#org637fe0d)
    1.  [Define Variables](#org4cc2c9c)
    2.  [Define Commands](#org1f6bbd7)
    3.  [Bind Commands](#org306bca7)
2.  [Autogen](#orgef2e001)
    1.  [Autogen](#org7f30227)
    2.  [Autobind](#orge0761fc)
3.  [Workflow](#org2fcaef8)
    1.  [Command options/Environment Variables](#org8ad0681)
    2.  [Live Builds](#orgc764ab4)

Okay, welcome to the rbs build system. To get started this build system is built
on the premise that it is a tool for people to write their own builds. This
means internals WILL be exposed but should not be used unless you have a really
good reason. There are also multiple ways or styles you can follow.


<a id="org637fe0d"></a>

# Manual

There are a bunch of examples in the examples directory but I want to take a
look at `example/cprog-simple/rbs-manual` first because thats the easiest
example to understand the fundementals of whats going on. The way rbs functions
is that you generate a command by putting information in a table, bind those
tables to a specific string which can be called by command line then parse the
command line. There are really 3 steps (4 if you count the last line).


<a id="org4cc2c9c"></a>

## Define Variables

For a simple example this is not needed because most of the variables will be
easy to understand. In `example/cprog-simple/rbs-manual` we just define the
compiler to use(note you can use things other then a compiler but we will get
into that), the files of the project and the binary name.

    local progname = "hello"
    local cc = "gcc"
    local cfiles = { "hello.c" }

This will produce a very basic command `gcc hello.c -o hello` which makes sense.
Obviously you could play around with this. Currently we only support gcc and
clang but plan to support multiple different languages/build systems.


<a id="org1f6bbd7"></a>

## Define Commands

Now that we have our information we can just throw that into a Command. In rbs a
command is literally just a table with a bunch of values which is turned into
shell/bat statements. You can make as many commands as you want. Each command
requires a .bin which tells rbs what to do(ie compile c). The other 2 fields are
also required which are input and output. Note input and output can be either a
single file or multiple files however input and output may be required to be the
same amount based on the .bin. This is so you can do stuff like pass a list of c
source code files to turn into object files.

    Bin = {}
    Bin.bin      = cc
    Bin.input    = cfiles
    Bin.output   = progname

After we do this we can call specific functions defined by rbs (or even make
your own) to do things with these files. b.exec is the main function you will
want to know about and is the one that generates the output files by running the
commands based on the information you gave it. So at this point if you just
wanted to compile cmd you can do `b.exec(Bin)`. There is also b.clean (deletes
output files) and b.install (installs output files to /usr/bin or argument).
Obviously you want to be able to do all this via command line so thats where the
next step comes into play.


<a id="org306bca7"></a>

## Bind Commands

Now we have the commands we can bind them to strings which we type into the
command line. By default running `./rbs-manual` will convert it to `./rbs-manual
all` so i would suggest you bind to the &ldquo;all&rdquo; argument like we did in the
example below.

    b.bind_exec(Bin, "all")
    b.bind_clean(Bin, "clean")
    b.bind_install(Bin, "install")
    
    b.run_arg(arg)

Notice b.bind<sub>exec</sub> just run b.exec on Bin? Well `b.bind_exec(Bin, "all")` is
just syntax sugar for `b.bind(Bin, "all", b.exec)`. The same thing is true for
bind<sub>install</sub> and bind<sub>clean</sub>. You can even make your own, you just need to make a
function that accepts a table (aka the command). FINALLY the b.run<sub>arg</sub>(arg) just
passes your command line arguments to rbs to be processed and to call any bound
functions if needed.


<a id="orgef2e001"></a>

# Autogen

Now that we know how to use rbs like a gnumake clone we can get rid of some of
the boiler plate. This syntax is inspired by meson/cmake but essentially this is
just better defaults so you don&rsquo;t have to worry about using 30 different build
tools and can just use 1 build file with sane defaults. First we can look at
`example/cprog-simple/rbs-autogen` but keep in mind each build system will have
different defaults.


<a id="org7f30227"></a>

## Autogen

Autogen is takes in a name, language/project default and a options table. This
options table will just let you overide any command table parameters for now.
For our example specifically we&rsquo;re given back 2 commands, one to create the o
files and one to link them into a binary. Keep in mind these are just commands
which are tables, so you can edit these after.

    local options = {cfiles = {"hello.c"}}
    
    compile, link = b.autogen("hello", "c", options)


<a id="orge0761fc"></a>

## Autobind

Autobind is to bind as autogen is to creating a command. It just provides simple
sane defaults.

    b.autobind(compile, "compile")
    b.autobind(link, "link", {install = true})
    
    b.run_arg(arg)

Autobind just binds the provided command to &ldquo;all&rdquo;, the given string, and clean.
If provided with an option it will then bind some extra stuff (in this example
&ldquo;install&rdquo;).


<a id="org2fcaef8"></a>

# Workflow


<a id="org8ad0681"></a>

## Command options/Environment Variables

Currently the command line flags and args are a work in progress. We currently
just loop through each argument, try to run it as a command and fail if it
doesn&rsquo;t exist.

@TODO(Renzix): document this when done


<a id="orgc764ab4"></a>

## Live Builds

Because the entire thing is written in lua you can just run a interpreter and
dont need to rerun the entire build system. This actually isn&rsquo;t hard at all and
only requires lua. You can just run `luajit -i rbs-autogen` or `lua -i
rbs-autogen`. Note that you can also run `b.run_arg({"all"})` to build all
things bound to &ldquo;all&rdquo;. I created a shorthand for this, with the `b.run("")`
function so I dont have to make tables every time. Keep in mind that anything
that is local to the lua file will NOT be pulled in so it is best practice to
keep things you want to be pulled into this interpreter as global. That is why
`b = require 'rbs'` is NOT `local b = require 'rbs'`.

    cprog-simple luajit -i rbs-autogen
    LuaJIT 2.0.5 -- Copyright (C) 2005-2017 Mike Pall. http://luajit.org/
    LOG: Creating build/obj
    JIT: ON CMOV SSE2 SSE3 SSE4.1 fold cse dce fwd dse narrow loop abc sink fuse
    >
    > b.exec(compile)
    LOG: cc -c  hello.c -o build/obj/hello.o
    > b.exec(link)
    LOG: cc   build/obj/hello.o -o build/hello
    > b.clean(compile)
    LOG: removing build/obj/hello.o
    > b.clean(link)
    LOG: removing build/hello
    >
    > b.run_arg({"all"})
    LOG: cc -c  hello.c -o build/obj/hello.o
    LOG: cc   build/obj/hello.o -o build/hello
    >
    > b.run "clean"
    LOG: removing build/obj/hello.o
    LOG: removing build/hello
    >
    > b.run "all"
    LOG: cc -c  hello.c -o build/obj/hello.o
    LOG: cc   build/obj/hello.o -o build/hello
    >

Once I remove the dependency on luafilesystem and figure out how to get io.popen
working I can tell people to use <https://github.com/Rosettea/Hilbish> with
`hilbish.runnerMode("hybridRev")`. @TODO(Renzix)

